clear
echo "--------------------------------------------------------------"
rm -R build/
rm -R tests/build/
mkdir -p build/views
mkdir -p tests/build
cp -R views build/
cd app/
cp *.* ../build
cd ../controllers/
cp *.* ../build
cd ../models/
cp *.* ../build
cd ../build
cp *.* ../tests/build
javac App.java
rm *.java
java App
