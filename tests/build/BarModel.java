public class BarModel extends Model {

	private String route;

	private String dir = "views/bar/images/";

	private String spartial;

	private String butt;

	private String rollover;

	BarModel(){

	}

	void init(Data data){

			whereami(data);

			data.fuse("reset","RESET");

			data.fuse("submit","SUBMIT");

			home(data);

			shop(data);

			property(data);

			buttonsroll(data);

			items(data);

			partial(data);

			prices(data);

	}

	public void prices(Data data){

		spartial = "price";

		data.smatrix(spartial,"Ref","Description","Price","QTY","Total");

		data.smatrix(spartial,"001","Sean Connery","99.99","1","00.00");

		data.smatrix(spartial,"002","George Lazenby","99.99","1","00.00");

		data.smatrix(spartial,"003","Roger Moore","99.99","1","00.00");

		data.smatrix(spartial,"004","Timothy Doalton","99.99","1","00.00");

		data.smatrix(spartial,"005","Pierce Brosnan","99.99","1","00.00");

		data.smatrix(spartial,"007","Daniel Craig","99.99","1","00.00");

		data.smatrix(spartial,"008","Peter Sellers","99.99","1","00.00");

		data.smatrix(spartial,"","","","","00.00");

		data.smatrix(spartial,"","","","","00.00");


	}

	public void home(Data data){

		spartial = "buttons";

		butt = dir + "buttons/ipad_button_default.jpg";

		data.spartial(spartial,"home",butt);

		data.spartial(spartial,"shop",butt);

		data.spartial(spartial,"property",butt);

		data.spartial(spartial,"financial",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

	}

	public void property(Data data){

		spartial = "property";

		data.spartial(spartial,"home-" + spartial,butt);

		data.spartial(spartial,"accomodation",butt);

		data.spartial(spartial,"landlords",butt);

		data.spartial(spartial,"students",butt);

		data.spartial(spartial,"inventory",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

	}

	public void shop(Data data){

		spartial = "shop";

		data.spartial(spartial,"home-" + spartial,butt);

		data.spartial(spartial,"mens",butt);

		data.spartial(spartial,"women",butt);

		data.spartial(spartial,"orders",butt);

		data.spartial(spartial,"dispatch",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

	}

	public void buttonsroll(Data data){

		spartial = "buttonsroll";

		rollover = dir + "buttons/default_rollover.jpg";

		data.spartial(spartial,"home",rollover);

		data.spartial(spartial,"shop",rollover);

		data.spartial(spartial,"property",rollover);

		data.spartial(spartial,"financial",rollover);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

		data.spartial(spartial,"",butt);

	}


	public void items(Data data){

		data.spartial("item","beats",dir + "/items/ipad_panel_beats.jpg");

		data.spartial("item","hyperadapt",dir + "/items/ipad_panel_hyperadapt.jpg");

		data.spartial("item","default",dir + "/items/ipad_panel_products.jpg");

		data.spartial("item","main",dir + "/items/ipad_panel.jpg");

	}

	public void partial(Data data){

		data.spartial("menu","Audio",dir + "ipad_panel_beats.jpg");

		data.spartial("menu","Products",dir + "ipad_button_products.jpg");

		data.spartial("menuroll","Products",dir + "ipad_button_products_rollover.jpg");

		data.fuse("signin",dir + "signin.jpg");

	}


}
