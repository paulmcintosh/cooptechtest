import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

public class EbankModel extends Model {

  private String route;

	private String dir = "views/bar/images/";

	private String spartial;

	private String butt;

	private String rollover;

  private String yyyy;

  private String mm;

  private String dd;

  private String method;

  private String noAfterDate;

  private String nextWorkingDay;

  private String currentDate;

  private String nextBusinessDay;

  private String paths;

  private String description;

  private String parameters;

  private String in;

  private String name;

  private String schema;

  private String type;

  private String format;

  private String responses;

  private String content;

  private String ErrorResponse;

  private String properties;

  private String code;

  private String message;

  private String compareNextDaytoBusinessdays;

  private String s;

  private int dayOfWeek;

  String[] week = {"SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"};

  String[] notBusinessDays = {"SATURDAY","SUNDAY"};


  EbankModel(){

    this.currentDate = "2019/11/2";

    this.method = "GET";

    this.paths = "next-working-day";

  }

  public void IncrementDateByOneDay(Data data){

    try {

      String dt = data.defuse("setDate");  // Start date

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

      Calendar c = Calendar.getInstance();

      c.setTime(sdf.parse(dt));

      c.add(Calendar.DATE, 1);  // number of days to add

      dt = sdf.format(c.getTime());

      System.out.println("IncrementDateByOneDay => " + dt);

      data.fuse("IncrementDateByOneDay",dt);

    } catch (ParseException e) {

        e.printStackTrace();

    }

  }

  public void setWorkingDays(Data data){

      data.fuse("mon","MONDAY");

      data.fuse("tue","TUESDAY");

      data.fuse("wed","WEDNESDAY");

      data.fuse("thu","THURSDAY");

      data.fuse("fri","FRIDAY");

  }

  public void setDate(Data data){

      data.fuse("setDate",yyyy + "-" + mm + "-" + dd);

  }

  public void getDayFromDateIncremented(Data data){

    try {
      String s = data.defuse("IncrementDateByOneDay");

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

      java.util.Date date = sdf.parse(s);

      Calendar c = Calendar.getInstance();

      c.setTime(date);

      int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

      setDayOfTheWeek(data,dayOfWeek);


    } catch (ParseException e) {

        e.printStackTrace();

    }
  }

  public void setDayOfTheWeek(Data data, int dayOfWeek){

    System.out.println("dayOfWeek => " + dayOfWeek);

    if(dayOfWeek == 7){

      dayOfWeek = 0;

    }

    data.fuse("getDayFromDateIncremented",week[dayOfWeek]);

  }

  public void getDayFromDate(Data data){

    try {
      String s = data.defuse("setDate");

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

      java.util.Date date = sdf.parse(s);

      Calendar c = Calendar.getInstance();

      c.setTime(date);

      int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

      data.fuse("getDayFromDate",week[dayOfWeek]);

    } catch (ParseException e) {

        e.printStackTrace();

    }
  }

  public void config(Data data){

    data.fuse("bank","Co-Op");

    data.fuse("url","http://www.example.com");

    data.fuse("year",yyyy);

    data.fuse("month",mm);

    data.fuse("day",dd);

  }

  public void ifNextDayNotBusinessDay(Data data){

    compareNextDaytoBusinessdays = "false";

    for(int i = 0; i < notBusinessDays.length; i++){

      if(data.defuse("getDayFromDateIncremented") == notBusinessDays[i]){

          System.out.println("getDayFromDateIncremented is notBusinessDays => " + notBusinessDays[i]);

          compareNextDaytoBusinessdays = notBusinessDays[i];


      }

    }

    data.fuse("ifNextDayNotBusinessDay",compareNextDaytoBusinessdays);
  }

  public void init(Data data){

    setWorkingDays(data);

    setDateRange(data);

    setDate(data);

    getDayFromDate(data);

    config(data);

    //TODO: Increment date by one day
    IncrementDateByOneDay(data);

    //TODO: Set day from Increment day
    getDayFromDateIncremented(data);

    //TODO: If date is working day set nextBusinessDay
    ifNextDayNotBusinessDay(data);

    //TODO: Set Tomorrow
    setTomorrow(data);

    //TODO: Get headers

    //TODO: GraphQL implementation


    ebank(data);

    outPut(data);

  }

  public void setTomorrow(Data data){

    if(data.defuse("ifNextDayNotBusinessDay") == "SATURDAY"){

      nextBusinessDay = "MONDAY";

    }else if(data.defuse("ifNextDayNotBusinessDay") == "SUNDAY"){

      nextBusinessDay = "MONDAY";

    }

    data.fuse("setTomorrow",nextBusinessDay);

  }

  public void outPut(Data data){

      //System.out.println("year =>" + data.defuse("year"));

      //System.out.println("month =>" + data.defuse("month"));

      //System.out.println("day =>" + data.defuse("day"));

      //System.out.println("mon =>" + data.defuse("mon"));

      System.out.println("setDate =>" + data.defuse("setDate"));

      System.out.println("getDayFromDate =>" + data.defuse("getDayFromDate"));

      System.out.println("getDayFromDateIncremented =>" + data.defuse("getDayFromDateIncremented"));

      System.out.println("ifNextDayNotBusinessDay =>" + data.defuse("ifNextDayNotBusinessDay"));

      System.out.println("nextBusinessDay => " + nextBusinessDay);

      //System.out.println("currentDate =>" + currentDate);

      //System.out.println("method =>" + method);

      //System.out.println("dayOfWeek =>" + dayOfWeek);

  }

  public void setDateRange(Data data){

    data.fuse("2019/11/1","FRIDAY");

    data.fuse("2019/11/2","SATURDAY");

    data.fuse("2019/11/3","SUNDAY");

    data.fuse("2019/11/4","MONDAY");

    data.fuse("2019/11/5","TUESDAY");

    data.fuse("2019/11/6","WEDNESDAY");

    data.fuse("2019/11/7","THURSDAY");
  }

  public void getDate(String yyyy, String mm, String dd){

    this.yyyy = yyyy;

    this.mm = mm;

    this.dd = dd;

  }

  public void ebank(Data data){

    spartial = "ebank";

    butt = dir + "buttons/ipad_button_default.jpg";

    data.spartial(spartial,"home-financial",butt);

    data.spartial(spartial,"timesheet",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

    data.spartial(spartial,"",butt);

  }

}
