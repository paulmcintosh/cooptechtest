import java.io.*;
import java.lang.*;
import java.util.*;

public class WriterModel extends Model {

  private Formatter x;

  WriterModel(){


  }

  public void closeFile(){

    x.close();

  }

  public void AddRecords(){

    x.format("%s%s%s","20 ","bucky ","roberts");

    x.format("%s%s%s","20 ","steve ","austin");

  }

  public void openFile(Data data){

    try {

      x = new Formatter(data.datas.get(1).value);

    }
    catch(Exception e){

    }

  }

  public void init(Data data){

    data.fuse("storage","views/bar/json/db.txt");

    openFile(data);

    AddRecords();

    closeFile();

  }

}
