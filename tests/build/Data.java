import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Data {

	public String key;

	public String value;

	public String part;

	public String ref;

	public String description;

	public String price;

	public String qty;

	public String total;

	String[] data = new String[3];

	List<Datas> datas = new ArrayList<Datas>();

	List<Partials> partials = new ArrayList<Partials>();

	List<Partials> menu = new ArrayList<Partials>();

	List<Partials> menuroll = new ArrayList<Partials>();

	List<Partials> buttons = new ArrayList<Partials>();

	List<Partials> shop = new ArrayList<Partials>();

	List<Partials> property = new ArrayList<Partials>();

	List<Partials> buttonsroll = new ArrayList<Partials>();

	List<Partials> slug = new ArrayList<Partials>();

	List<Partials> item = new ArrayList<Partials>();

	List<Partials> ebank = new ArrayList<Partials>();

	List<Matrix> prices = new ArrayList<Matrix>();

	private String name;


	Data(){

	}

	public void fusion(String name){

		this.name = name;

	}


	public void smatrix(String part,String ref, String description, String price, String qty, String total){

		if(part == "price"){

	 		prices.add(new Matrix(ref,description,price,qty,total));

	 	}

	}


	public void spartial(String part,String key,String value){

		if(part == "menu"){

			menu.add(new Partials(key,value));

		} else if(part == "buttons"){

			buttons.add(new Partials(key,value));

		} else if(part == "menuroll"){

			menuroll.add(new Partials(key,value));

		} else if(part == "slug"){

			slug.add(new Partials(key,value));

		} else if(part == "item"){

			item.add(new Partials(key,value));

		} else if(part == "buttonsroll"){

			buttonsroll.add(new Partials(key,value));

		} else if(part == "shop"){

			shop.add(new Partials(key,value));

		} else if(part == "property"){

			property.add(new Partials(key,value));

		} else if(part == "ebank"){

			ebank.add(new Partials(key,value));

		}

	}

	public void fuse(String key,String value){

		datas.add(new Datas(key,value));

	}

	public String slugs(String key){

		String value = "false";

		for(int i = 0; i < slug.size(); i++){

			if(slug.get(i).key == key){

					 value = slug.get(i).value;

			}

		}

		return value;

	}

	public String defuse(String key){

		String value = "false";

		for(int i = 0; i < datas.size(); i++){

			if(datas.get(i).key == key){

					 value = datas.get(i).value;

			}

		}

		return value;

	}

	public String fission(){

		return name;

	}

}
