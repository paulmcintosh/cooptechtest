public class Controller {

	public String model;

	private String name;

	public String controller;

	public void go(String controller,Data data){

    View view = new View();

		if(controller == "bar"){

			Bar con = new Bar(data);

			BarModel model = new BarModel();

			model.init(data);

			view.display(data,"desktop");

		} else if(controller == "home"){

			Home con = new Home(data);

			HomeModel model = new HomeModel(data);

			model.init(data);

			view.display(data,"desktop");

		} else if(controller == "about"){

			About con = new About(data);

			AboutModel model = new AboutModel(data);

			model.init(data);

			view.display(data,"desktop");

		}

	}


	void init(Data data){

		data.fuse("Scott","Summers");

		data.fuse("Charles","Xavier");

		data.fuse("Jean","Grey");

	}

	public void fusion(){

//		foos.add(new Foo("Bruce","Wayne"));

	}

}
