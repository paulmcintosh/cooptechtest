import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Desktop extends JFrame implements ActionListener{

  private ImageIcon img;

  private JLabel label;

  public void actionPerformed(ActionEvent e) {

  }

  Desktop(Data data){

      dashboard(data);

  }


  public void dashboard(Data data){

    setTitle("Online Products Catalogue");

    Container contents = getContentPane();

    contents.setLayout(new FlowLayout(2,0,0));

    //TODO: North
    JPanel north = new JPanel();

    img = new ImageIcon(getClass().getResource(data.datas.get(0).value));

    label = new JLabel(img);

    north.add(label);


    menu(contents,data);

    //TODO: Center
    JPanel center = new JPanel();

    center.setLayout(new FlowLayout(1,0,0));

    img = new ImageIcon(getClass().getResource(data.datas.get(1).value));

    label = new JLabel(img);

    center.add(label);

    contents.add(center);


    setDefaultCloseOperation(EXIT_ON_CLOSE);

    pack();


  }


  public void menu(Container contents,Data data){

    //TODO: West
    JPanel west = new JPanel();

    for(int counter = 0; counter<11; counter++){

        img = new ImageIcon(getClass().getResource(data.datas.get(0).value));

        JButton but = new JButton("",img);

        but.addActionListener(this);

        but.setPreferredSize(new Dimension(232, 70));

        but.setBorderPainted(false);

        but.setContentAreaFilled(false);

        but.setFocusPainted(false);

        but.setOpaque(false);

        west.add(but);

        west.setLayout(new GridLayout(0,1));

    }

    contents.add(west,BorderLayout.WEST);

  }

}
