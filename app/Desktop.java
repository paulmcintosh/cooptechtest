import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Desktop extends JFrame implements ActionListener{

  private ImageIcon img;

  private JLabel label;

  private JTextField txt1 = new JTextField(20);

  private JTextField txt2 = new JTextField(20);

  private JTextField txt3 = new JTextField(20);

  private  JTextField spiderman;

  private  JTextField wolverine;

  private  JTextField cyclops;

  String thisPage;

  String thisId;

  JTextField[]text = new JTextField[11];

  JButton[]butt = new JButton[11];

  int size = 4;

  String route;

  public void menuHome(ActionEvent e, Data data, Controller controller){

    if(thisPage == "dashboard" && thisId == "home"){

      if(e.getSource() == butt[1]){

        route = "bar";

        controller.surl(data,route,"dashboard","shop");

    		controller.go(route,data);

        dispose();

      }

      if(e.getSource() == butt[2]){

        route = "bar";

        controller.surl(data,route,"dashboard","property");

    		controller.go(route,data);

        dispose();

      }

      if(e.getSource() == butt[3]){

        route = "ebank";

        controller.surl(data,route,"ebank","timesheet");

    		controller.go(route,data);

        dispose();

      }

    }

  }

  public void actionPerformed(ActionEvent e) {

    System.out.println(thisPage);

    System.out.println(thisId);

    Data  data = new Data();

		Controller controller = new Controller();

    menuHome(e,data,controller);

    if(e.getSource() == butt[0]){

      route = "bar";

      controller.surl(data,route,"dashboard","home");

      controller.go(route,data);

      dispose();

    }


  }

  Desktop(Data data){

      thisPage = data.slugs("page");

      thisId = data.slugs("id");

      shop(data);

      property(data);

      //TODO: Base Page
      dashboard(data);

      ebank(data);

  }

  public void ebank(Data data){

    if(data.slug.get(1).value == "ebank"){

      setTitle("E-Bank");

      Container contents = getContentPane();

      contents.setLayout(new FlowLayout(2,0,0));

      menu(contents,data);

      JPanel center = new JPanel();

      center.setLayout(new FlowLayout(1,0,0));

      item(data);

      fizzFinancial(data,center);

      contents.add(center);

      setDefaultCloseOperation(EXIT_ON_CLOSE);

      pack();

    }

  }

  public void shop(Data data){

    if(data.slugs("page") == "dashboard" && data.slugs("id") == "shop"){

      setTitle("Property");

      Container contents = getContentPane();

      contents.setLayout(new FlowLayout(2,0,0));

      menu(contents,data);

      JPanel center = new JPanel();

      center.setLayout(new FlowLayout(1,0,0));

      item(data);

      fizz(data,center);

      contents.add(center);

      setDefaultCloseOperation(EXIT_ON_CLOSE);

      pack();

    }

  }

  public void property(Data data){

    if(data.slugs("page") == "dashboard" && data.slugs("id") == "property"){

      setTitle("Property");

      Container contents = getContentPane();

      contents.setLayout(new FlowLayout(2,0,0));

      menu(contents,data);

      JPanel center = new JPanel();

      center.setLayout(new FlowLayout(1,0,0));

      item(data);

      fizz(data,center);

      contents.add(center);

      setDefaultCloseOperation(EXIT_ON_CLOSE);

      pack();

    }

  }

  public void visionnaries(Data data,JPanel bane, int i){

    visionShop(data,bane,i);

    //visionFinancial(data,bane,i);

  }

  public void visionFinancial(Data data,JPanel bane, int i){

    if(thisId == "timesheet"){

      bane.add(new JLabel("Year"));

      bane.add(new JLabel("Month"));

      bane.add(new JLabel("Day"));

      courierFinancial(data,bane,i);

    }

  }

  public void visionShop(Data data,JPanel bane, int i){

    if(thisId == "shop"){

      bane.add(new JLabel(data.prices.get(i).ref));

      bane.add(new JLabel(data.prices.get(i).description));

      bane.add(new JLabel(data.prices.get(i).price));

      courier(data,bane,i);

    }

  }

  public void setResetTimesheet(Data data,JPanel bane, int i){

    JButton reset = new JButton();

    reset.addActionListener(new ActionListener(){

       public void actionPerformed(ActionEvent ae){

          Data  data = new Data();

          Controller controller = new Controller();

          route = "ebank";

          controller.surl(data,route,"ebank","timesheet");

          controller.go(route,data);

          dispose();


       }

    });

    reset.add(new JLabel(data.defuse("reset")));

    bane.add(reset);

  }

  public void setReset(Data data,JPanel bane, int i){

    JButton reset = new JButton();

    reset.addActionListener(new ActionListener(){

       public void actionPerformed(ActionEvent ae){

          Data  data = new Data();

          Controller controller = new Controller();

          route = "bar";

          controller.surl(data,route,"dashboard","shop");

          controller.go(route,data);

          dispose();


       }

    });

    reset.add(new JLabel(data.defuse("reset")));

    bane.add(reset);

  }


  public void setSubmitTimesheet(Data data,JPanel bane, int i){

    JButton submit = new JButton();

    submit.addActionListener(new ActionListener(){

       public void actionPerformed(ActionEvent ae){

          String formDay = spiderman.getText();

          String formMonth = wolverine.getText();

          String formYear = cyclops.getText();

          Data  data = new Data();

          data.fuse("formDay",formDay);

          data.fuse("formMonth",formMonth);

          data.fuse("formYear",formYear);

          Controller controller = new Controller();

          route = "ebankRequest";

          controller.surl(data,route,"ebank","timesheet");

          controller.go(route,data);

          dispose();


       }

    });

    submit.add(new JLabel(data.defuse("submit")));

    bane.add(submit);

  }

  public void setSubmit(Data data,JPanel bane, int i){

    JButton submit = new JButton();

    submit.addActionListener(new ActionListener(){

       public void actionPerformed(ActionEvent ae){

          Data  data = new Data();

          Controller controller = new Controller();

          route = "bar";

          controller.surl(data,route,"dashboard","shop");

          controller.go(route,data);

          dispose();


       }

    });

    submit.add(new JLabel(data.defuse("submit")));

    bane.add(submit);

  }

  public void courierFinancial(Data data,JPanel bane, int i){

    if( i == 9){

        setReset(data,bane,i);

        setSubmit(data,bane,i);

    } else if( i == 0){

        bane.add(new JLabel(data.prices.get(i).qty));

        bane.add(new JLabel(data.prices.get(i).total));

    } else {

      text[i] = new JTextField(data.prices.get(i).qty);

      bane.add(text[i]);

      bane.add(new JTextField(data.prices.get(i).total));

    }

  }

  public void courier(Data data,JPanel bane, int i){

    if( i == 9){

        setReset(data,bane,i);

        setSubmit(data,bane,i);

    } else if( i == 0){

        bane.add(new JLabel(data.prices.get(i).qty));

        bane.add(new JLabel(data.prices.get(i).total));

    } else {

      text[i] = new JTextField(data.prices.get(i).qty);

      bane.add(text[i]);

      bane.add(new JTextField(data.prices.get(i).total));

    }

  }

  public void QueryResult(Data data, JPanel bane){

    if(data.defuse("RequestResult") == "true"){

      bane.add(new JLabel("Day Entered: " + data.defuse("getDayFromDate")));

      bane.add(new JLabel("Next Day: " + data.defuse("getDayFromDateIncremented")));

      bane.add(new JLabel("Next Business Day: " + data.defuse("setTomorrow")));


    }
  }

  public void fizzFinancial(Data data,JPanel center){

    JLabel label = new JLabel(img);

    label.setLayout( new BorderLayout() );

    JPanel bane = new JPanel();

    bane.setOpaque(false);

    bane.setLayout(new GridLayout(0,3));

    QueryResult(data,bane);

    bane.add(new JLabel("Day"));

    bane.add(new JLabel("Month"));

    bane.add(new JLabel("Year"));

    spiderman = new JTextField("06");

    wolverine = new JTextField("11");

    cyclops = new JTextField("2019");

    bane.add(spiderman);

    bane.add(wolverine);

    bane.add(cyclops);

    bane.add(new JLabel(""));

    int i =0;

    setResetTimesheet(data,bane,i);

    setSubmitTimesheet(data,bane,i);

    label.add( bane );

    center.add(label);

  }

  public void fizz(Data data,JPanel center){

    JLabel label = new JLabel(img);

    label.setLayout( new BorderLayout() );

    JPanel bane = new JPanel();

    bane.setOpaque(false);

    bane.setLayout(new GridLayout(0,5));

    for(int i = 0; i < 10; i++){

      visionnaries(data,bane,i);

    }

    label.add( bane );

    center.add(label);

  }

  public void login(Data data){

    if(data.slug.get(1).value == "login"){

      setTitle("Online Products Catalogue");

      Container contents = getContentPane();

      contents.setLayout(new FlowLayout(1,0,0));

      JPanel north = new JPanel();

      img = new ImageIcon(getClass().getResource(data.datas.get(0).value));

      label = new JLabel(img);

      north.add(label);

      contents.add(north);

      setDefaultCloseOperation(EXIT_ON_CLOSE);

      pack();

    }

  }



  public void dashboard(Data data){

    if(data.slugs("page") == "dashboard" && data.slugs("id") == "home"){

      setTitle("Dashboard");

      Container contents = getContentPane();

      contents.setLayout(new FlowLayout(2,0,0));

      menu(contents,data);

      //TODO: Center
      JPanel center = new JPanel();

      center.setLayout(new FlowLayout(1,0,0));

      item(data);

      label = new JLabel(img);

      center.add(label);

      contents.add(center);


      setDefaultCloseOperation(EXIT_ON_CLOSE);

      pack();

    }

  }

  public void item(Data data){

    System.out.println("Page Id => " + thisId);

    if(thisId == "home"){

      img = new ImageIcon(getClass().getResource(data.item.get(0).value));

    }

    if(thisId == "shop"){

      img = new ImageIcon(getClass().getResource(data.item.get(3).value));

    }

    if(thisId == "property"){

      img = new ImageIcon(getClass().getResource(data.item.get(2).value));

    }

    if(thisId == "timesheet"){

      img = new ImageIcon(getClass().getResource(data.item.get(3).value));

    }

  }

  public void page(JButton[] butt, Data data, int counter){

    if(data.slug.get(2).value == "timesheet"){

      butt[counter].add(new JLabel(data.ebank.get(counter).key));

    } else if(data.slug.get(2).value == "property"){

      butt[counter].add(new JLabel(data.property.get(counter).key));

    } else if(data.slug.get(2).value == "shop"){

      butt[counter].add(new JLabel(data.shop.get(counter).key));

    } else {

        butt[counter].add(new JLabel(data.buttonsroll.get(counter).key));

    }



  }

  public void menu(Container contents,Data data){

    //TODO: West
    JPanel west = new JPanel();

    for(int counter = 0; counter<11; counter++){

        img = new ImageIcon(getClass().getResource(data.buttons.get(counter).value));

        butt[counter] = new JButton("",img);

        img = new ImageIcon(getClass().getResource(data.buttonsroll.get(counter).value));

        page(butt,data,counter);

        butt[counter].setRolloverIcon(img);

        butt[counter].addActionListener(this);

        butt[counter].setPreferredSize(new Dimension(232, 70));

        butt[counter].setBorderPainted(false);

        butt[counter].setContentAreaFilled(false);

        butt[counter].setFocusPainted(false);

        butt[counter].setOpaque(false);

        west.add(butt[counter]);

        west.setLayout(new GridLayout(0,1));

    }

    contents.add(west,BorderLayout.WEST);

  }

}
