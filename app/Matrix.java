public class Matrix{

    public String ref;

    public String description;

    public String price;

    public String qty;

    public String total;

    public Matrix(String ref, String description, String price, String qty, String total){

        this.ref = ref;

        this.description = description;

        this.price = price;

        this.qty = qty;

        this.total = total;
    }
}
