### READ ME ###

This application employs the 'boiler plate' approach to java development. Thus striving to be
totally agnostic development environment. Not beholding to a particular IDE, packages, etc. The emphasis is on building the most robust application while side-stepping 'on-trend' developmental decisions that may have futre ramafications.

## Micro Services##

* Launch the application from the build folder and run
```
java App

```

## Application GUI ##

* Once the application has opened use the side menu

* From the side menu click on the financial button

* You will be presented with the Timesheet page. The form presented will accept the day/month/year

* Additional information will be outputted to the terminal

## Micro Services Example files: Controller and Model##
```
/controllers/Ebank.java

/controllers/EbankRequest.java

/models/EbankModel.java

```

## views ##
* All views are generated from the view folder

## Micro Services Controller##
* Example below
```

  Ebank foo = new Ebank();

```

## Java MVC Framework ##

* This application follows the MVC Framework. By enforcing single responsibility through the
use of controllers to orchestrate models and views.

* Create your controller in the controllers folder *

* Create your model in the models folder *

* Use the examples in the corresponding folders *

* Update the models/Model.java go() method with the new controller & Model *

## IMPORTANT ##

** All files are transfered to the build folder. So if writing to a file make
sure you look in the build folder**

## Call a controller ##
```
route = "bar";

data.fusion("This controller is " + route + " baby");

controller.surl(data,route,"fashion","mens");

controller.go(route,data);

```

## Get route, page and id ##
```
  data.slugs("route");

  data.slugs("page");

  data.slugs("id");

```

## Change Desktop background ##
```
public void item(Data data){

  if(data.slug.get(2).value == "ebank"){

      img = new ImageIcon(getClass().getResource(data.item.get(1).value));

  }
}

```

## Change Desktop menu ##
```
public void page(JButton[] butt, Data data, int counter){

  if(data.slug.get(2).value == "property"){

    butt[counter].add(new JLabel(data.property.get(counter).key));

  }
}

```

## Tests ##
* Are located in the the tests folder. All tests are built manually in the build folder. If you are unsure of building your tests. The please refer to the build.sh file. This will give you invaulable pointers in running your tests. Please remember to add the revelvant packages.Example below
```

  //Data responsibility
  Data data = new Data();

  //Data exposure
  data.fuse("foo","bar");

  // Object instaniation
  TestEbank testEbank = new TestEbank(data);

  //Test query result
  testEbank.testQueryResult(data);

  // Test date
  testEbank.testgetDayFromDate(data);

```
